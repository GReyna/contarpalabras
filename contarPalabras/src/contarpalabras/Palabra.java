package contarpalabras;
public class Palabra {
  public String palabra ;
    public int frecuencia;
   //public String espacios; 
    
   public Palabra(String nom, int num/* String e*/){
        this.palabra= nom;  //NOMBRE DE LA CANCION
       this.frecuencia = num;
      // this.espacios= e ; 
    }


   

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public int getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(int frecuencia) {
        this.frecuencia = frecuencia;
    }
    /*  public String getEspacios() {
        return espacios;
    }

    public void setEspacios(String espacios) {
        this.espacios = espacios;
    }
    */
    @Override
   public String toString(){
       return  palabra+" : "+this.frecuencia+"\n";
               
    }
  
    
}
